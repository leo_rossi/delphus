package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = LanguageEntity.TABLE_NAME)
public class LanguageEntity {

    public static final String TABLE_NAME = "DEL_LANGUAGE";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

}
