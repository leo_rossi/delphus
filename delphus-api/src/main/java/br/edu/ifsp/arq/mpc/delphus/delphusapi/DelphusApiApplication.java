package br.edu.ifsp.arq.mpc.delphus.delphusapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class DelphusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DelphusApiApplication.class, args);
	}

}
