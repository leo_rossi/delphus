package br.edu.ifsp.arq.mpc.delphus.delphusapi.enums;

public enum WorkspaceType {
    COMPANY,
    OSS_PROJ,
    EDUCATION;

}
