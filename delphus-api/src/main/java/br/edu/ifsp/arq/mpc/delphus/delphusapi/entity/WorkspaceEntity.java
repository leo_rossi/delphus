package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import br.edu.ifsp.arq.mpc.delphus.delphusapi.enums.WorkspaceType;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@Entity
public class WorkspaceEntity {

    public static final String TABLE_NAME = "DEL_WORKSPACE";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String owner;

    private WorkspaceType type;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "DEL_WORKSPACE_DEV",
        joinColumns = @JoinColumn(name = "dev_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "workspace_id", referencedColumnName = "id"))
    private Set<DeveloperEntity> collaborators;

}
