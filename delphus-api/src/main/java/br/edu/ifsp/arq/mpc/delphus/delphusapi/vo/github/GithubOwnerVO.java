package br.edu.ifsp.arq.mpc.delphus.delphusapi.vo.github;

import lombok.Data;

@Data
public class GithubOwnerVO {

    private Long id;

    private String login;

}
