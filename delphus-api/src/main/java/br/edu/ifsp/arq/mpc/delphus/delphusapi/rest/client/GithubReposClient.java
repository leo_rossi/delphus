package br.edu.ifsp.arq.mpc.delphus.delphusapi.rest.client;

import br.edu.ifsp.arq.mpc.delphus.delphusapi.vo.github.GithubRepoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@FeignClient(url = "https://api.github.com", name = "github")
public interface GithubReposClient {

    @RequestMapping(method = RequestMethod.GET, value = "/users/{user}/repos")
    List<GithubRepoVO> getRepos(@PathVariable("user") final String user);

    @RequestMapping(method = RequestMethod.GET, value = "/repos/{user}/{repo}")
    GithubRepoVO getRepo(@PathVariable("user") final String user, @PathVariable("repo") final String repo);

    @RequestMapping(method = RequestMethod.GET, value = "/repos/{user}/{repo}/languages")
    Map<String, Long> getLanguages(@PathVariable("user") final String user, @PathVariable("repo") final String repo);

}
