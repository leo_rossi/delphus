package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Data
@Entity
@Table(name = DeveloperEntity.TABLE_NAME)
public class DeveloperEntity {
    public static final String TABLE_NAME = "DEL_DEVELOPER";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @OneToMany(mappedBy = "owner")
    private Set<ProjectEntity> projects;
}
