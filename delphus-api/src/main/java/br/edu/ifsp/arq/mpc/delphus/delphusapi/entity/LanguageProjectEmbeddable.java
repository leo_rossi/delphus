package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class LanguageProjectEmbeddable implements Serializable {

    private Long languageId;

    private Long projectId;
}
