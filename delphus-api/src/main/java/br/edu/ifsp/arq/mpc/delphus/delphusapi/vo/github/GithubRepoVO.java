package br.edu.ifsp.arq.mpc.delphus.delphusapi.vo.github;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GithubRepoVO {
    private Long id;

    private String name;

    private String fullName;

    private GithubOwnerVO owner;

    @JsonAlias("private")
    private Boolean privateRepo;

    private String description;

    @JsonAlias("html_url")
    private String htmlUrl;

    private String url;

    @JsonAlias("collaborators_url")
    private String collabsUrl;

    @JsonAlias("languages_url")
    private String languagesUrl;

    private String language;

    @JsonAlias("default_branch")
    private String defaultBranch;

    private Long size;

    @JsonAlias("created_at")
    private LocalDateTime createdAt;

    @JsonAlias("updated_at")
    private LocalDateTime updatedAt;

    @JsonAlias("pushed_at")
    private LocalDateTime pushedAt;

}
