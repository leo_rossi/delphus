package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = LanguageProjectEntity.TABLE_NAME)
public class LanguageProjectEntity {

    public static final String TABLE_NAME = "DEL_PROJECT_LANG";

    @EmbeddedId
    private LanguageProjectEmbeddable id;

    private Long lines;
}
