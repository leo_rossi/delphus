package br.edu.ifsp.arq.mpc.delphus.delphusapi.rest.controller;

import br.edu.ifsp.arq.mpc.delphus.delphusapi.rest.client.GithubReposClient;
import br.edu.ifsp.arq.mpc.delphus.delphusapi.vo.github.GithubRepoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/repos")
public class ReposController {

    @Autowired
    private GithubReposClient reposClient;

    @GetMapping("/{user}")
    public List<GithubRepoVO> getAll(@PathVariable("user") final String user) {
        return reposClient.getRepos(user);
    }

    @GetMapping("/{user}/{repo}")
    public GithubRepoVO getRepo(@PathVariable("user") final String user, @PathVariable("repo") final String repo) {
        return reposClient.getRepo(user, repo);
    }

    @GetMapping("/{user}/{repo}/languages")
    public Map<String, Long> getLanguages(@PathVariable("user") final String user, @PathVariable("repo") final String repo) {
        return reposClient.getLanguages(user, repo);
    }
}
