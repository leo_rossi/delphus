package br.edu.ifsp.arq.mpc.delphus.delphusapi.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Data
@Entity
@Table(name = ProjectEntity.TABLE_NAME)
public class ProjectEntity {
    public static final String TABLE_NAME = "DEL_PROJECT";

    @Id
    @Column(name = "ID_PROJECT")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PROJECT_NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "id")
    private DeveloperEntity owner;

    @OneToMany(mappedBy = "id.projectId")
    private Set<LanguageProjectEntity> langs;

}
