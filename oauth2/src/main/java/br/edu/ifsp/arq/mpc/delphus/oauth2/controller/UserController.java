package br.edu.ifsp.arq.mpc.delphus.oauth2.controller;

import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.request.RegistrationRequest;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.response.UserResponse;
import br.edu.ifsp.arq.mpc.delphus.oauth2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/me")
    public Principal me(final Principal principal) {
        return principal;
    }

    @GetMapping("/{id}")
    public UserResponse findById(@PathVariable final Long id) {
        return userService.findById(id);
    }

    @GetMapping("/me/info")
    public UserResponse userInfo() {
        return userService.findUserInfo();
    }

    @PostMapping
    public ResponseEntity<?> register(@RequestBody @Valid final RegistrationRequest registrationRequest) {
        Long id = userService.register(registrationRequest);
        URI uri = URI.create("/users/" + id);
        return ResponseEntity.created(uri).build();
    }

}
