package br.edu.ifsp.arq.mpc.delphus.oauth2.entity.response;

import lombok.Data;

import java.util.Set;

@Data
public class UserResponse {

    private Long id;

    private String username;

    private String name;

    private Set<String> roles;
}
