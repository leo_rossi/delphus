package br.edu.ifsp.arq.mpc.delphus.oauth2.service;

import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.request.RegistrationRequest;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.response.UserResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    Long register(RegistrationRequest registration);

    UserResponse findUserInfo();

    UserResponse findById(Long id);

}
