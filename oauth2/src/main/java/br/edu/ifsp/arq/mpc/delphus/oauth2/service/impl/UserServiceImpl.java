package br.edu.ifsp.arq.mpc.delphus.oauth2.service.impl;

import br.edu.ifsp.arq.mpc.delphus.oauth2.service.UserService;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.UserEntity;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.request.RegistrationRequest;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.response.UserResponse;
import br.edu.ifsp.arq.mpc.delphus.oauth2.mapper.UserMapper;
import br.edu.ifsp.arq.mpc.delphus.oauth2.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;


@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Long register(final RegistrationRequest registration) {
        log.info("M=register, registration={}", registration);

        UserEntity userEntity = userMapper.registrationToUser(registration);
        userEntity.setPassword(passwordEncoder.encode(registration.getPassword()));

        userRepository.save(userEntity);

        return userEntity.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public UserResponse findUserInfo() {
        log.info("M=findUserInfo");

        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userMapper.userToResponse(user);
    }

    @Override
    public UserResponse findById(Long id) {
        log.info("M=findById, id={}", id);
        UserEntity user = userRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        return userMapper.userToResponse(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("M=loadUserByUsername, username={}", username);

        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
    }
}
