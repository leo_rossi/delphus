package br.edu.ifsp.arq.mpc.delphus.oauth2.entity.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class RegistrationRequest {

    @Email
    @NotBlank
    private String username;

    @NotBlank
    private String name;

    @NotBlank
    @Size(min = 8)
    @ToString.Exclude
    private String password;
}
