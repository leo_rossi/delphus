package br.edu.ifsp.arq.mpc.delphus.oauth2.repository;

import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    @Query(" from #{#entityName} u " +
            " left join fetch u.authorities r " +
            " where u.username = :username ")
    Optional<UserEntity> findByUsername(@Param("username") final String username);
}
