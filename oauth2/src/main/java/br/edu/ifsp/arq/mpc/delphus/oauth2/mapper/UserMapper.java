package br.edu.ifsp.arq.mpc.delphus.oauth2.mapper;

import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.RoleEntity;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.UserEntity;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.request.RegistrationRequest;
import br.edu.ifsp.arq.mpc.delphus.oauth2.entity.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "authorities", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true)
    })
    UserEntity registrationToUser(final RegistrationRequest registration);

    @Mapping(target = "roles", source = "authorities")
    UserResponse userToResponse(final UserEntity user);

    default String roleToString(final RoleEntity role) {
        return role.getAuthority();
    }
}
