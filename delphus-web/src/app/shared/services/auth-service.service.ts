import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpUrlEncodingCodec } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static URL = environment.auth_api_url;

  constructor(private http: HttpClient) { }

  public login (user): Promise<any> {
    return new Promise((resolve, reject) => {
      if (user.username && user.password) {
        const httpOpts = {
          headers: new HttpHeaders({
            'Authorization': 'Basic ' + environment.authorization_bearer,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        this.http
          .post(AuthService.URL, JSON.stringify(user), httpOpts)
          .subscribe(res => resolve(res));
      } else {
        reject(new Error('Preencha os campos'));
      }
    });
  }
}
